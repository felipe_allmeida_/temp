﻿namespace Totem.Enum
{
    public enum Cards
    {
        NONE,
        HUD,
        ARTIST,
        ARTIST_ABOUT,
        ARTIST_CURIOSITY,
        FOUNDATION,
        FOUNDATION_INSTITUTION,
        FOUNDATION_BUILDING,
        FOUNDATION_CURIOSITY,
        EXHIBITION,
        SCHEDULE,
        COLLECTION,
        VISIT_US,
        CULTURAL_CENTER
    }
    public enum CardType
    {
        NONE,
        HUD,
        A_FUNDACAO,
        O_ARTISTA,
        PROGRAMACAO,
        ACERVO
    }
}
