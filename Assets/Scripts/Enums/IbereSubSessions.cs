﻿namespace Totem.Enum
{
    public enum IbereSubSessions
    {
        ARTISTA_SOBRE,
        ARTISTA_CURIOSIDADE,
        FUNDACAO_INSTITUICAO,
        FUNDACAO_CONSTRUCAO,
        FUNDACAO_CURIOSIDADE
    }
}
