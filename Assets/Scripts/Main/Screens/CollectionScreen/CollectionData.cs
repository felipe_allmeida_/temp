﻿using UnityEngine;

public class CollectionData
{
    public string title;
    public string description;
    public string artist;
    public Vector2 dimension;
    public Sprite sprite;
}
