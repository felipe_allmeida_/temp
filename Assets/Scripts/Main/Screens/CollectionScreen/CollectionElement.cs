﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CollectionElement : MonoBehaviour
{
    public Action<CollectionData> onClick;
    
    public string title;
    public string description;
    public string artist;

    public Vector2 dimension;

    public CollectionData data;

    public Text collectionTitle;
    public Image collectionImage;

    public Button button;

    public void Init()
    {
        data = new CollectionData()
        {
            title = title,
            description = description,
            artist = artist,
            dimension = dimension,
            sprite = collectionImage.sprite
        };

        button.onClick.AddListener(() => { onClick?.Invoke(data); });
    }
}
