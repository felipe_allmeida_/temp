﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CollectionView : MonoBehaviour
{
    public Action onClick;
    
    public Image image;
    public Text title;
    public Text description;
    public Text artist;
    public Button button;

    public void Init()
    {
        button.onClick.AddListener(() => { onClick?.Invoke(); });
    }

    public void Show(CollectionData data)
    {
        image.sprite = data.sprite;
        image.rectTransform.sizeDelta = data.dimension;
        title.text = data.title;
        description.text = data.description;
        artist.text = data.artist;
    }
}
