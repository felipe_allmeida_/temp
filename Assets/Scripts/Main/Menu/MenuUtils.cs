﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuUtils : MonoBehaviour
{
    private static MenuUtils _instance;
    public static MenuUtils Instance
    {
        get
        {
            return _instance;
        }
    }

    public Sprite buttonArtist;
    public Sprite buttonFundation;
    public Sprite buttonExhibition;
    public Sprite buttonCollection;
    public Sprite buttonVisitUs;
    public Sprite buttonCulturalCenter;

    private void Start()
    {
        DontDestroyOnLoad(gameObject);
        _instance = this;
    }
}
