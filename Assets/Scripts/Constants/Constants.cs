﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Constants
{
    public const float NAV_BAR_ANIMATION_TIME = .25f;
    public const float CARD_FADE_IN = .35f;
    public const float CARD_FADE_OUT = 0.25f;
}
