﻿using Framework;
using System.Collections.Generic;
using Totem.Enum;
using UnityEngine;
using UnityEngine.UI;

namespace Totem
{
    public class ConfigState : State<MainScene.StateType>
    {
        [SerializeField] private GameObject _listContentObject;
        [SerializeField] private GameObject _toggleGroupObject;
        [SerializeField] private Button _btnStart;

        public override void T_Init()
        {
            base.T_Init();

            _btnStart.onClick.AddListener(ButtonStart_OnClick);
        }

        public override void T_Enable()
        {
            base.T_Enable();
            gameObject.SetActive(true);
        }

        public override void T_Disable()
        {
            base.T_Disable();
            gameObject.SetActive(false);
        }

        private void ButtonStart_OnClick()
        {
            SaveConfiguration();

            stateMachine.ChangeToState(MainScene.StateType.TOTEM);
        }

        private void SaveConfiguration()
        {
            Dictionary<CardType, ConfigData> dictSessionConfig = new Dictionary<CardType, ConfigData>();

            for (int i = 0; i < _listContentObject.transform.childCount; i++)
            {
                var content = _listContentObject.transform.GetChild(i).GetComponent<ConfigContent>();

                ConfigData config = new ConfigData()
                {
                    index = content.index,
                    isOn = content.toggle.isOn,
                    session = content.session
                };

                if (content.listChildren.IsNullOrEmpty() == false)
                {
                    config.listSubConfig = new List<ConfigData>();

                    foreach (var childConfig in content.listChildren)
                    {
                        if (childConfig.toggle.interactable == false) continue;

                        ConfigData childData = new ConfigData()
                        {
                            isOn = childConfig.toggle.isOn,
                            session = childConfig.session,
                            subSession = childConfig.subSession
                        };

                        config.listSubConfig.Add(childData);
                    }

                    if (config.listSubConfig.Exists(child => child.isOn) == false)
                    {
                        config.isOn = false;
                    }
                }

                dictSessionConfig[config.session] = config;
            }

            Languages selectedLanguage = Languages.PT_BR;

            for (int i = 0; i < _toggleGroupObject.transform.childCount; i++)
            {
                var config = _toggleGroupObject.transform.GetChild(i).GetComponent<LanguageContent>();
                if (config.Toggle.isOn)
                {
                    selectedLanguage = config.Language;
                    break;
                }
            }
            
            ConfigurationManager.Instance.Init(selectedLanguage, dictSessionConfig);
        }
    }
}