﻿using System.Collections;
using System.Collections.Generic;
using Totem.Enum;
using UnityEngine;
using UnityEngine.UI;

namespace Totem
{
    public class ConfigContent : MonoBehaviour
    {
        public Toggle toggle;
        public CardType session;
        public string subSession;
        public List<ConfigContent> listChildren;
        public int index;

        private void Start()
        {
            if (listChildren != null)
            {
                toggle.onValueChanged.AddListener((bool isOn) =>
                {
                    for (int i = 0; i < listChildren.Count; i ++)
                    {
                        listChildren[i].toggle.isOn = isOn;
                        listChildren[i].toggle.onValueChanged.AddListener((bool childIsOn) =>
                        {
                            if (childIsOn && !toggle.isOn)
                            {
                                toggle.isOn = true;
                            }
                        });
                    }
                    listChildren.ForEach(child => child.toggle.isOn = isOn);
                });
            }
        }
    }
}
