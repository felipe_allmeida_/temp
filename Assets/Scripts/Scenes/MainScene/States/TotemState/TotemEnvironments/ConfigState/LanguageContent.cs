﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Totem
{
    public class LanguageContent : MonoBehaviour
    {
        public Toggle Toggle;
        public Languages Language;
    }
}
