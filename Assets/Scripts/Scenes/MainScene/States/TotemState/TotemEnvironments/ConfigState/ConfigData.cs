﻿using System.Collections;
using System.Collections.Generic;
using Totem.Enum;
using UnityEngine;

namespace Totem
{
    public struct ConfigData
    {
        public int index { get; set; }
        public bool isOn { get; set; }
        public CardType session;
        public string subSession;
        public List<ConfigData> listSubConfig;
    }
}