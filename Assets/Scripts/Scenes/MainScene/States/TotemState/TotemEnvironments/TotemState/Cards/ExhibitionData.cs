﻿using UnityEngine;
using UnityEngine.UI;

public class ExhibitionData
{
    public string collectionName;
    public string artistOrCuratorshipOrOrganization;
    public string descriptionShort;
    public string descritptionLong;
    public string dateExhibition;
    public Sprite image;
}
