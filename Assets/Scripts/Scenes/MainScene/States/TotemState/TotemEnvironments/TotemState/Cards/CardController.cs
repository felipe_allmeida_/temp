﻿using Framework;
using System;
using System.Collections;
using System.Collections.Generic;
using Totem.Enum;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

namespace Totem
{
    public class CardController : MonoBehaviour
    {
        #region UI
        [SerializeField] private Image _animationBlocker;
        [SerializeField] private NavigationBar _navBar;
        [SerializeField] private RectTransform _canvasRectTransform;
        [SerializeField] private List<BaseCard> listCards;
        #endregion

        #region World
        [SerializeField] private VideoPlayer _videoPlayerBackground;
        #endregion

        private Dictionary<CardType, BaseCard> dictCards = new Dictionary<CardType, BaseCard>();
        private BaseCard _currentCard = null;
        private TimerNodule _timerNodule;

        public void T_Update()
        {
            if (_currentCard == null) return;
            if (Input.anyKeyDown || Input.touchCount > 0)
            {
                if (_currentCard.CardType != CardType.HUD)
                {
                    _timerNodule?.Stop();
                    _timerNodule = Timer.WaitSeconds(15, () =>
                    {
                        _navBar.Hide();
                        ChangeCard(_currentCard?.CardType ?? CardType.NONE, CardType.HUD);
                    });
                }
            }
        }

        public void Init()
        {
        }

        public void T_Enable()
        {
            listCards.ForEach(x =>
            {
                x.OnBtnChangeCardClicked = Card_OnBtnChangeCardClicked;
                x.Init(_canvasRectTransform.sizeDelta);
                dictCards[x.CardType] = x;
            });

            _navBar.Enable(CardType.HUD);
            _navBar.onClickNavButton = (current, next) => ChangeCard(current, next, null);
            _navBar.Hide();
            ChangeCard(CardType.NONE, CardType.HUD);
        }

        private void ChangeCard(CardType currentCard, CardType nextCard, Action onChangeCard = null)
        {
            if (_currentCard?.CardType == nextCard)
                throw new ArgumentOutOfRangeException($"The current card is already {nextCard}");

            if (!dictCards.ContainsKey(nextCard))
                throw new NotImplementedException(nextCard.ToString());

            if (nextCard == CardType.HUD)
                _animationBlocker.enabled = false;
            else
                _animationBlocker.enabled = true;

            Action onHideCard = () =>
            {
                _currentCard = dictCards[nextCard];

                UpdateWorldBackgroundToCard(nextCard);

                _currentCard.Show();

                if (nextCard != CardType.HUD)
                {
                    _navBar.Show(nextCard);
                    _timerNodule?.Stop();
                    _timerNodule = Timer.WaitSeconds(15, () =>
                    {
                        _navBar.Hide();
                        ChangeCard(_currentCard?.CardType ?? CardType.NONE, CardType.HUD);
                    });
                }
                else
                {
                    _navBar.Hide();
                }

                onChangeCard?.Invoke();
            };

            if (_currentCard != null)
            {
                _currentCard.Hide(onHideCard);
            }
            else
            {
                onHideCard.Invoke();
            }
        }

        private void UpdateWorldBackgroundToCard(CardType nextCard)
        {
            switch (nextCard)
            {
                case CardType.HUD:
                    _videoPlayerBackground.Stop();
                    _videoPlayerBackground.Play();
                    break;
                default:
                    _videoPlayerBackground.Stop();
                    break;
            }
        }

        private void Card_OnBtnChangeCardClicked(CardType currentCard, CardType nextCard)
        {
            Debug.Log(nextCard.ToString());
            ChangeCard(currentCard, nextCard);
        }
    }
}
