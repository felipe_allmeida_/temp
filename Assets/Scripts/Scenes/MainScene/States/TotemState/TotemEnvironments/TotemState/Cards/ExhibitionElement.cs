﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ExhibitionElement : MonoBehaviour
{
    public string _collectionName;
    public string _artistOrCuratorshipOrOrganization;
    public string _descriptionShort;
    public string _descritptionLong;
    public string _dateExhibition;

    public Action<ExhibitionData> onClick;

    public ExhibitionData data;

    public DateTime _dateTo;

    public Button _button;

    public Image _image;

    public void Init()
    {
        data = new ExhibitionData()
        {
            collectionName = _collectionName,
            artistOrCuratorshipOrOrganization = _artistOrCuratorshipOrOrganization,
            descriptionShort = _descriptionShort,
            descritptionLong = _descritptionLong,
            dateExhibition = _dateExhibition,
            image = _image.sprite
        };

        _button.onClick.AddListener(() =>
        {
            onClick?.Invoke(data);
        });
    }
}
