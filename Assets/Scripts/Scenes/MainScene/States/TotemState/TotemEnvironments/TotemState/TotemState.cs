﻿using Framework;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Totem
{
    public class TotemState : State<MainScene.StateType>
    {
        [SerializeField] private CardController _cardController;

        public override void T_Init()
        {
            base.T_Init();
            _cardController.Init();
        }

        public override void T_Enable()
        {
            base.T_Enable();
            _cardController.T_Enable();
            gameObject.SetActive(true);
        }

        public override void T_Disable()
        {
            base.T_Disable();
            gameObject.SetActive(false);
        }

        public override void T_Update()
        {
            base.T_Update();
            _cardController.T_Update();
        }
    }
}
