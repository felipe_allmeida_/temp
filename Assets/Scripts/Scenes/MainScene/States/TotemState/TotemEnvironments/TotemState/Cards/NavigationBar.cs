﻿using Framework;
using System;
using System.Collections;
using System.Collections.Generic;
using Totem.Enum;
using UnityEngine;

namespace Totem
{
    public class NavigationBar : MonoBehaviour
    {
        public Action<CardType, CardType> onClickNavButton;
        public GameObject menuButtonPrefab;
        [SerializeField] private GameObject content;
        private bool _isVisible = false;
        private CardType currentSession;
        private RectTransform _rectTransform;
        private CanvasGroup _canvasGroup;
        private TweenNodule _tweenNodule;
        private Dictionary<CardType, MenuButton> _dictButtons = new Dictionary<CardType, MenuButton>();
        public void Enable(CardType startSession)
        {
            _rectTransform = GetComponent<RectTransform>();
            _canvasGroup = GetComponent<CanvasGroup>();
            currentSession = startSession;
            foreach (var session in ConfigurationManager.Instance.GetEnabledSessions())
            {
                var menuButton = Instantiate(menuButtonPrefab, content.transform).GetComponent<MenuButton>();

                menuButton.Init(session);
                menuButton.onClick = MenuButton_OnClick;
                _dictButtons.Add(session, menuButton);
            }
            Animate(0);
        }

        public void Show(CardType session)
        {
            if (_isVisible) return;
            _isVisible = true;
            gameObject.SetActive(true);
            currentSession = session;
            if (_dictButtons.ContainsKey(currentSession))
                _dictButtons[currentSession].Select(true);
            _tweenNodule?.Stop();
            _tweenNodule = Tween.FloatTo(0f, 1f, Constants.NAV_BAR_ANIMATION_TIME, TweenEase.LINEAR, Animate);
        }

        public void Hide()
        {
            if (!_isVisible) return;
            _isVisible = false;
            _tweenNodule?.Stop();
            _tweenNodule = Tween.FloatTo(1f, 0f, Constants.NAV_BAR_ANIMATION_TIME, TweenEase.LINEAR, Animate);
            _tweenNodule.onFinished += () => gameObject.SetActive(false);
        }

        private void Animate(float p_progress)
        {
            _canvasGroup.alpha = p_progress;
            //_rectTransform.anchoredPosition = new Vector2(-_rectTransform.rect.width * (1f - p_progress), 0f);
        }

        private void MenuButton_OnClick(CardType session)
        {
            var lastSession = currentSession;
            currentSession = session;
            
            if (_dictButtons.ContainsKey(lastSession))
                _dictButtons[lastSession].Select(false);

            if (_dictButtons.ContainsKey(currentSession))
                _dictButtons[currentSession].Select(true);

            onClickNavButton?.Invoke(lastSession, currentSession);
        }
    }

}
