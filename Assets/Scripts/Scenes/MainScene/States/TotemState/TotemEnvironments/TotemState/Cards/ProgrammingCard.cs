﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Totem
{
    public class ProgrammingCard : BaseCard
    {
        public GameObject listView;
        public ExhibitionView exhibitionView;

        public List<ExhibitionElement> exhibitionElements;

        public override void Show(Action callbackFinish = null)
        {
            exhibitionElements.ForEach(x => {
                x.Init();
                x.onClick = ExhibitionElement_OnClick;
            });
            exhibitionView.onClick = ExhibitionView_OnClick;
            ExhibitionView_OnClick();
            base.Show(callbackFinish);
        }
        void ExhibitionElement_OnClick(ExhibitionData data)
        {
            listView.gameObject.SetActive(false);
            exhibitionView.gameObject.SetActive(true);
            exhibitionView.Init(data);
        }
        void ExhibitionView_OnClick()
        {
            exhibitionView.gameObject.SetActive(false);
            listView.gameObject.SetActive(true);
        }
    }
}
