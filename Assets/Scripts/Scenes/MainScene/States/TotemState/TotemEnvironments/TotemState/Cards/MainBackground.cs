﻿using Framework;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainBackground : MonoBehaviour
{
    public float scrollSpeed = 0.5f;

    private Renderer quadRenderer;

    // Start is called before the first frame update
    void Start()
    {
        quadRenderer = GetComponent<Renderer>();
    }

    // Update is called once per frame
    void Update()
    {
        Vector2 textureOffset = new Vector2(Time.time * scrollSpeed, Time.time * (scrollSpeed / 2f));
        quadRenderer.material.mainTextureOffset = textureOffset;
    }
}
