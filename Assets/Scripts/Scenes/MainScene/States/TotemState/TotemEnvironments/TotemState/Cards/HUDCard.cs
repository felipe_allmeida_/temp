﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Framework;
using System;
using Totem.Enum;

namespace Totem
{
    public class HUDCard : BaseCard
    {
        public GameObject _btnHUDPrefab;
        public List<HUDButton> btnList;

        [SerializeField] private GameObject _content;
        [SerializeField] private Image _animationLocker;
        [SerializeField] private Image _background;
        private CardType selectedCard = CardType.NONE;
        public override void Init(Vector2 canvasDimension)
        {
            base.Init(canvasDimension);
            var enabledSessions = ConfigurationManager.Instance.GetEnabledSessions();

            foreach (var btn in btnList)
            {
                if (!enabledSessions.Contains(btn.Session))
                {
                    btn.gameObject.SetActive(false);
                }
                else
                {
                    btn.Init();
                    btn.onClick = (nextCard) =>
                    {
                        selectedCard = nextCard;
                        ChangeCard(nextCard);
                    };
                }
            }

            _animationLocker.color = Color.black;
        }

        public override void Show(Action callbackFinish = null)
        {
            _animationLocker.color = Color.black;

            base.Show(callbackFinish);
        }

        protected override void Animate(float p_progress, bool isShow)
        {
            foreach (var btn in btnList)
            {
                if (p_progress <= .5)
                {
                    if (btn.Session != selectedCard)
                    {
                        btn.Animate(p_progress * 2);
                    }
                }
                else
                {
                    if (btn.Session == selectedCard)
                    {
                        var progress = (p_progress - 0.5f) * 2;
                        btn.Animate(progress);

                        _background.color = new Color(progress, progress, progress, 1f);
                    }
                }
            }

            //switch (CardState)
            //{
            //    case State.VISIBLE:
            //        _animationLocker.color = new Color(_animationLocker.color.r, _animationLocker.color.g, _animationLocker.color.b, 1 - p_progress);
            //        break;
            //    case State.HIDDEN:
            //        if (p_progress > .5f)
            //        {
            //            _animationLocker.color = new Color(_animationLocker.color.r, _animationLocker.color.g, _animationLocker.color.b, .5f * (1f - p_progress) + .5f);
            //        }
            //        break;
            //}

            //base.Animate(p_progress, isShow);
        }
    }
}
