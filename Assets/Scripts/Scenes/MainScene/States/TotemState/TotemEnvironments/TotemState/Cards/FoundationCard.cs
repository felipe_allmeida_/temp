﻿using System;
using UnityEngine;
using UnityEngine.UI;
using Totem.Enum;

namespace Totem
{
    public class FoundationCard : BaseCard
    {
        [SerializeField] private Image _background;

        [SerializeField] private RectTransform institutionContent;
        [SerializeField] private RectTransform buildingContent;

        public override void Init(Vector2 canvasDimension)
        {
            base.Init(canvasDimension);
        }

        public override void Show(Action callbackFinish = null)
        {
            institutionContent.gameObject.SetActive(ConfigurationManager.Instance.IsSubSessionEnabled(CardType.A_FUNDACAO, "Instituição"));
            buildingContent.gameObject.SetActive(ConfigurationManager.Instance.IsSubSessionEnabled(CardType.A_FUNDACAO, "Construção"));

            float totalHeight = 200;

            if (institutionContent.gameObject.activeSelf)
                totalHeight += institutionContent.rect.height;

            if (buildingContent.gameObject.activeSelf)
                totalHeight += buildingContent.rect.height;

            _scrollRectContent.sizeDelta = new Vector2(0f, totalHeight);

            base.Show(callbackFinish);
        }

        protected override void Animate(float p_progress, bool isShow)
        {
            base.Animate(p_progress, isShow);

            _background.color = new Color(_background.color.r, _background.color.g, _background.color.b, 1f - p_progress);
        }
    }
}