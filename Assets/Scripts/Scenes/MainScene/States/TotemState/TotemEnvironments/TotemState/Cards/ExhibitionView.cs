﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ExhibitionView : MonoBehaviour
{
    public Action onClick;
    
    public Transform content;
    public Text title;
    public Image image;
    public Text date;
    public Text description;
    public Text artistOrCuratorshipOrOrganization;
    public Button returnButton;

    void Start()
    {
        returnButton.onClick.AddListener(() => { onClick?.Invoke(); });
    }

    public void Init(ExhibitionData data)
    {
        content.position = new Vector3(content.position.x, 0, content.position.z);

        title.text = data.collectionName;
        image.sprite = data.image;
        date .text = data.dateExhibition;
        description.text = data.descritptionLong;

        switch(title.text)
        {
            case "Spider(Aranha)":
                description.text = "Depois de permanecer por pouco mais de duas décadas abrigada em regime de comodato ao lado do Museu de Arte Moderna, no Parque Ibirapuera, em São Paulo, Spider, da artista francesa Louise Bourgeois, " +
                    "que integra a Coleção Itaú Cultural, chega pela primeira vez ao Rio Grande do Sul para uma temporada de dois meses na Fundação Iberê." +
                    "\n\r\n\rA parceria com o Itaú Cultural vem de longa data, e foi retomada em maio de 2018 com a exposição Moderna para Sempre – Fotografia Modernista Brasileira na Coleção Itaú Cultural, um recorte de 144 obras fotográficas de " +
                    "importantes artistas do movimento modernista brasileiro, dos quais 60 nunca haviam estado antes em Porto Alegre. Agora, a Fundação Iberê tem a honra de receber a famosa aranha gigante de Bourgeois. A escultura é um “autorretrato” " +
                    "da infância da artista, de seus traumas e a relação com a mãe, a quem considerava sua melhor amiga e alguém que a protegeu de diversas formas." +
                    "\n\r\n\rBourgeois (França, 1911 – Estados Unidos, 2010) cresceu em um ateliê de restauro de tapeçarias. Com a morte da mãe, em 1932, a jovem abandonou os estudos em matemática para transformar suas experiências em uma linguagem " +
                    "visual altamente pessoal, por meio do uso de imagens mitológicas e arquetípicas, adotando objetos como espirais, gaiolas, ferramentas médicas e as famosas aranhas para simbolizar a psique feminina, a beleza e a dor psicológica. " +
                    "Spider tem mais de 3 metros de altura em bronze e está equilibrada em oito pernas, com terminações que remetem à agulha e ao bordado." +
                    "\n\r\n\r“A aranha é protetora, a nossa protetora contra os mosquitos. […] A outra metáfora é que a aranha representa a mãe. A minha mãe era a minha melhor amiga. Ela era inteligente, paciente, tranquilizadora, delicada, " +
                    "trabalhadora, indispensável e, sobretudo, ela era tecelã – como a aranha. Para mim, as aranhas não são aterradoras.” [Louise Bourgeois: Sculptures, Environnements, Dessins, 1938-1995]" +
                    "\n\r\n\rSpider reafirma a nossa parceria com a Coleção Itaú Cultural e o nosso compromisso de trazer a Porto Alegre o que há de mais instigante e inquieto na arte moderna no Brasil e no mundo. As aranhas de Bourgeois " +
                    "já passaram por países como Alemanha, Argentina, Bélgica, Canadá, Coreia do Sul, Dinamarca, Espanha, Estados Unidos, França, Inglaterra, Japão, México, Qatar, Rússia, Suíça e Estocolmo.";
                break;
            case "FestFoto 2019":
                description.text = "A 12a edição do Festival Internacional de Fotografia de Porto Alegre – 27 de abril a 26 de maio de 2019 – aborda a produção fotográfica contemporânea relacionada às dispersões populacionais e " +
                    "conflitos identitários, envolvidas tanto em problema de disputas de nacionalidades e etnias quanto no âmbito de dilemas pessoais. A dita crise põe em cheque a ideia de identidade como instância fixa, apontando " +
                    "para formatos mais fluídos e flutuantes. No trânsito do tornar-se, a localização não ocorre por uma referência prévia e predeterminada, mas pela reconstrução e transformação de identidades históricas herdadas de " +
                    "um suposto passado comum. A ideia do híbrido se impõe como realidade na produção de referências nacionais, raciais e étnicas se é que estes conceitos ainda são úteis como descrição da realidade contemporânea e de " +
                    "formas identitárias que não são mais integralmente nenhuma das originais, embora tenha traços delas." +
                    "\n\r\n\rPara dar conta do tema, a curadoria do Festival organizou os trabalhos em três eixos temático: deslocamentos e diásporas de populações por fatores políticos, econômicos ou crises ambientais; o híbrido " +
                    "como resultado do contato e resistência entre pessoas; a reconfiguração de sentidos pela dispersão de imagens." +
                    "\n\r\n\rA exposição coletiva de fotografias em grandes dimensões reunirá obras de artistas brasileiros e estrangeiros, selecionados a partir da visita e participação dos curadores em eventos internacionais. " +
                    "Entre os destaques, teremos o projeto Humanae, da artista carioca radicada na Espanha, Angelica Dass, e Paisagem Movediça, de Bruno Bernardi, sobre o deslocamento dos moradores do distrito de Bento Ribeiro, " +
                    "em Mariana (MG) provocado pelo rompimento da barragem da Samarco-Vale, e Patagônia em Conflito, projeto em andamento do argentino Pablo Piovano sobre as disputas pela propriedade de terra no território do povo " +
                    "Mapuche. A mostra também apresenta casos emblemáticos de deslocamentos humanos como o êxodo dos africanos no chamado Genocídio em Ruanda, fotografado por Américo Mariano na década de 90. O caso recente dos " +
                    "colombianos, que após duas décadas de guerra interna, buscam no uma chance de prosperidade em Antofogasta (Chile), também será apresentado pelo fotógrafo chileno Cristian Ochoa no ensaio Sueño Sudamericano." +
                    "\n\r\n\rA programação expositiva é composta ainda, pelos 20 finalistas do Fotograma Livre, concurso internacional promovido pelo FestFoto e LensCulture, com a exibição de seus trabalhos em formato digital e " +
                    "impressos. O concurso recebeu inscrições de artistas e fotógrafos de 19 países e tem como objetivo estimular o desenvolvimento da linguagem fotográfica multimídia e ampliar o intercâmbio entre os países. " +
                    "O concurso foi vencido pelo Coletivo VEO, formado por Bruno Crocianelli (Argentina), Flavio Edreira (Brasil), Eleonora Ferri (Itália), Jean-Matthieu Gosselin (França), Maria Munzi (Argentina), " +
                    "Ullic Narducci (Itália), Nathalie Vigini (Suíça). No trabalho Veinte Pequeños Azulejos Grises, o grupo armou um mosaico para explorar a ideia de dispersão embutida no conceito de diáspora.";
                break;
            case "Iberê Camargo: visões da Redenção":
                description.text = "Iberê Camargo: visões da Redenção" +
                    "\n\r\n\r“Há gente caminhando dentro de mim…”, escreveu Iberê. Pessoas em movimento, na cena da vida – na obra do artista." +
                    "\n\r\n\rNo início da década de 1980, após 40 anos vivendo no Rio de Janeiro, Iberê Camargo retornou a Porto Alegre e passou a frequentar o Parque Farroupilha – mais conhecido como Parque da Redenção. " +
                    "Nas caminhadas ao lado de sua esposa Maria Coussirat Camargo, observava a paisagem e o ir e vir de frequentadores como transeuntes, artistas, ciclistas e pessoas em situação de vulnerabilidade. " +
                    "Esses anônimos, transpostos com potência expressiva para seus desenhos, pinturas e gravuras, logo se transformaram em arquétipos de um pintor que enfrentava a dor e a realidade do mundo." +
                    "\n\r\n\rIberê expressava as formas da natureza e da condição humana, “atingidas pela vida”, por meio de árvores fantasmagóricas e de personagens que habitavam a cena, sem rumo. Suas anotações que, à primeira vista, " +
                    "aparentavam ser simples registros da vida cotidiana, ganharam um significado maior, transformando-se em matéria-prima para algumas das obras mais famosas do artista, como a série Ciclistas." +
                    "\n\r\n\rO parque mais tradicional da cidade – e palco das mais diversas manifestações sociais, culturais e políticas – revelou-se, então, como um portal, um deslocamento da realidade para outra ordem no tempo. " +
                    "Delírio e devaneio – um novo estar no mundo." +
                    "\n\r\n\rAssim era Iberê Camargo e seu duplo – o ciclista –, que caminha dentro dele assim como a outra gente do Parque da Redenção.";
                break;
        }
    }
}
