﻿using System;
using System.Collections;
using System.Collections.Generic;
using Totem.Enum;
using UnityEngine;

namespace Totem
{
    public interface ICard
    {        
        void Show(Action callbackFinish = null);
        void Hide(Action callbackFinish = null);
    }
}
