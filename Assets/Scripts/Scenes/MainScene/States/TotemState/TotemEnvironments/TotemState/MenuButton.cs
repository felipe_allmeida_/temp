﻿using System;
using Totem.Enum;
using UnityEngine;
using UnityEngine.UI;

namespace Totem
{
    public class MenuButton : MonoBehaviour
    {
        public Action<CardType> onClick;

        [SerializeField] private Button _button;
        [SerializeField] private Image _buttonImage;
        [SerializeField] private Text _buttonText;
        public CardType CardType { get; private set; }

        public void Init(CardType cardType)
        {
            CardType = cardType;
            switch(CardType)
            {
                case CardType.A_FUNDACAO:
                    _buttonImage.sprite = MenuUtils.Instance.buttonFundation;
                    _buttonText.text = "FUNDAÇÃO";
                    break;
                case CardType.O_ARTISTA:
                    _buttonImage.sprite = MenuUtils.Instance.buttonArtist;
                    _buttonText.text = "ARTISTA";
                    break;
                case CardType.PROGRAMACAO:
                    _buttonImage.sprite = MenuUtils.Instance.buttonExhibition;
                    _buttonText.text = "AGENDA";
                    break;
                case CardType.ACERVO:
                    _buttonImage.sprite = MenuUtils.Instance.buttonCollection;
                    _buttonText.text = "ACERVO";
                    break;
            }

            _button.onClick.AddListener(() => onClick?.Invoke(CardType));

            Select(false);
        }

        public void Select(bool isSelected)
        {
            _buttonImage.color = isSelected ? Color.white : Color.gray;
            _buttonText.color = isSelected ? Color.white : Color.gray;
        }
    }
}