﻿using System;
using System.Collections;
using System.Collections.Generic;
using Totem.Enum;
using UnityEngine;
using UnityEngine.UI;

namespace Totem
{
    public class HUDButton : MonoBehaviour
    {
        public Action<CardType> onClick;
        public CardType Session { get { return _session; } }
        [SerializeField] private Text _btnText;
        [SerializeField] private Image _btnImage;
        [SerializeField] private CardType _session;
        public bool Enabled { get; private set; } = false;

        public void Init()
        {
            Debug.Log(this);
            Enabled = true;
            GetComponent<Button>().onClick.AddListener(() => onClick?.Invoke(_session));
        }

        public void Animate(float progress)
        {
            _btnText.color = new Color(_btnText.color.r, _btnText.color.g, _btnText.color.b, progress);
            _btnImage.color = new Color(_btnImage.color.r, _btnImage.color.g, _btnImage.color.b, progress);
        }
    }
}