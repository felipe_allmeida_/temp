﻿using Framework;
using System;
using System.Collections;
using System.Collections.Generic;
using Totem.Enum;
using UnityEngine;

namespace Totem
{
    public abstract class BaseCard : MonoBehaviour, ICard
    {     
        public enum State
        {
            VISIBLE,
            HIDDEN
        }

        [SerializeField] protected RectTransform _scrollRectContent;

        [SerializeField] private CardType _cardType;

        public CardType CardType { get { return _cardType; } }
        public State CardState { get; private set; } = BaseCard.State.HIDDEN;

        public Action<CardType, CardType> OnBtnChangeCardClicked;


        protected Vector2 _canvasDimension;
        protected CanvasGroup _canvasGroup;
        protected RectTransform _rectTransform;

        private Action _OnShowAnimationFinished;
        private Action _OnHideAnimationFinished;
        private TweenNodule animationNode;
        private TweenNodule afterShowAnimationNode;

        public virtual void Init(Vector2 canvasDimension)
        {
            _rectTransform = GetComponent<RectTransform>();
            _canvasGroup = GetComponent<CanvasGroup>();

            if (_scrollRectContent != null)
            {
                _scrollRectContent.position = new Vector3(_scrollRectContent.position.x, 0f, _scrollRectContent.position.z);
            }

            _canvasDimension = canvasDimension;
            gameObject.SetActive(false);
        }

        public virtual void Hide(Action callbackFinish = null)
        {
            if (CardState == State.HIDDEN) return;

            _OnHideAnimationFinished = callbackFinish;
            animationNode?.Stop();
            animationNode = Tween.FloatTo(1f, 0f, Constants.CARD_FADE_OUT, TweenEase.LINEAR, (value) => Animate(value, false));
            animationNode.onFinished += Hide_OnFinished;
        }

        public virtual void Show(Action callbackFinish = null)
        {
            if (CardState == State.VISIBLE) return;

            _OnShowAnimationFinished = callbackFinish;
            animationNode?.Stop();
            animationNode = Tween.FloatTo(0f, 1f, Constants.CARD_FADE_IN, TweenEase.LINEAR, (value) => Animate(value, true));
            animationNode.onFinished += Show_OnFinished;

            Timer.WaitFrames(1, () => gameObject.SetActive(true));
        }                

        protected virtual void Animate(float p_progress, bool isShow)
        {
            if (isShow)
                _rectTransform.anchoredPosition = new Vector2(_canvasDimension.x*2 * (1 - p_progress), _rectTransform.anchoredPosition.y);
            else
                _rectTransform.anchoredPosition = new Vector2(-_canvasDimension.x*2 * (1 - p_progress), _rectTransform.anchoredPosition.y);
        }

        protected void ChangeCard(CardType next)
        {
            OnBtnChangeCardClicked?.Invoke(CardType, next);
        }

        protected virtual void Show_OnFinished()
        {
            CardState = State.VISIBLE;

            if (_scrollRectContent != null && _scrollRectContent.position.y > 10f)
            {
                float previousPositionY = _scrollRectContent.position.y;
                afterShowAnimationNode?.Stop();
                afterShowAnimationNode = Tween.FloatTo(0f, 1f, .25f, TweenEase.LINEAR, (value) =>
                {
                    _scrollRectContent.position = new Vector3(_scrollRectContent.position.x, _scrollRectContent.position.y * (1f - value), _scrollRectContent.position.z);
                });
            }

            _OnShowAnimationFinished?.Invoke();
        }

        protected virtual void Hide_OnFinished()
        {
            gameObject.SetActive(false);
            CardState = State.HIDDEN;
            _OnHideAnimationFinished?.Invoke();
        }
    }
}
