﻿using Framework;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Totem
{
    public class MainScene : Scene<MainScene.StateType>
    {
        public enum StateType
        {
            CONFIG,
            TOTEM
        }

        public override void AInitialize()
        {
            base.AInitialize();
        }
    }
}
