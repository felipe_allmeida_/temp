﻿using Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using Totem.Enum;
using UnityEngine;

namespace Totem
{
    public class ConfigurationManager : MonoBehaviour
    {
        private static ConfigurationManager _instance;
        public static ConfigurationManager Instance
        {
            get
            {
                if (_instance == null)
                {
                    GameObject obj = new GameObject("ConfigurationManager");
                    DontDestroyOnLoad(obj);
                    _instance = obj.AddComponent<ConfigurationManager>();
                }
                return _instance;
            }
        }


        public Languages SelectedLanguage { get; private set; }
        private Dictionary<CardType, ConfigData> _dictSessionConfig;

        public void Init(Languages language, Dictionary<CardType, ConfigData> dictSessionConfig)
        {
            SelectedLanguage = language;
            _dictSessionConfig = dictSessionConfig;
        }

        public CardType GetStartSession()
        {
            return _dictSessionConfig.Where(x => x.Value.index == 0).First().Value.session;
        }

        public List<CardType> GetEnabledSessions()
        {
            return _dictSessionConfig.Where(x => x.Value.isOn).Select(x => x.Value.session).ToList();
        }

        public bool IsSessionEnabled(CardType session)
        {
            if (_dictSessionConfig.ContainsKey(session))
            {
                return _dictSessionConfig[session].isOn;
            }

            return false;
        }

        public bool IsSubSessionEnabled(CardType session, string subSession)
        {
            if (_dictSessionConfig.ContainsKey(session))
            {
                var childConfig = _dictSessionConfig[session].listSubConfig.FirstOrDefault(x => x.subSession == subSession);

                if (!string.IsNullOrEmpty(childConfig.subSession))
                {
                    return childConfig.isOn;
                }
            }

            return false;
        }
    }
}